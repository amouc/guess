package com.amou.guess

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class MaterialActivityTest{
    @Rule
    @JvmField
    val activityTestRule = ActivityTestRule<MaterialActivity>(MaterialActivity::class.java)

    @Test
    fun guessWrong(){
        val resource = activityTestRule.activity.resources
        val secret = activityTestRule.activity.secretNumber.secret
        for (n in 1..10) {
            //val n = 5
            if (n != secret) {
                onView(withId(R.id.ed_number)).perform(clearText())
                onView(withId(R.id.ed_number)).perform(typeText(n.toString()))
                onView(withId(R.id.button_ok)).perform(click())
                val message =
                    if (n < secret) resource.getString(R.string.bigger)
                    else resource.getString(R.string.smaller)
                onView(withText(message)).check(matches(isDisplayed()))
                onView(withText(resource.getString(R.string.ok))).perform(click())

            }
        }
        onView(withId(R.id.button_fab)).perform(click())
        onView(withText("OK")).perform(click())


    }

    @Test
    fun fabButtonTest(){
        val resource = activityTestRule.activity.resources
        val n = 99
        onView(withId(R.id.ed_number)).perform(clearText())
        onView(withId(R.id.ed_number)).perform(typeText(n.toString()))
        onView(withId(R.id.button_ok)).perform(click())
        //按下彈跳視窗 OＫ按鈕
        onView(withText(R.string.ok)).perform(click())

        //關閉軟體鍵盤
       onView(withId(R.id.button_fab)).perform(closeSoftKeyboard())
        //按下重玩按鈕
        onView(withId(R.id.button_fab)).perform(click())
        onView(withText(R.string.ok)).perform(click())
        //比對counter 是否為 0
        onView(withId(R.id.counter)).check(matches(withText("0")))

    }


}