package com.amou.guess

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    val secretNumber = SecretNumber()
    val TAG = MainActivity::class.java.simpleName
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d(TAG,"secret:${secretNumber.secret}")

    }

    fun check(view : View){
        val number = ed_number.text.toString().toInt()
        println("number:$number")
        Log.d(TAG,"number:$number")
        val diff = secretNumber.validate(number)
        var message = getString(R.string.yes_you_got_it)
        if (diff>0){
            message = getString(R.string.smaller)
        }else if(diff<0){
            message = getString(R.string.bigger)
        }
        //Toast.makeText(this,message,Toast.LENGTH_LONG).show()
        AlertDialog.Builder(this)
            .setTitle(getString(R.string.dialog_title))
            .setMessage(message)
            .setPositiveButton(R.string.ok,null)
            .show()
    }
}

