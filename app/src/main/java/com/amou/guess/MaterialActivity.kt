package com.amou.guess

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity;

import kotlinx.android.synthetic.main.activity_material.*
import kotlinx.android.synthetic.main.content_material.*

class MaterialActivity : AppCompatActivity() {
    val secretNumber = SecretNumber()
    val TAG = MaterialActivity::class.java.simpleName
    companion object{
        val REQUEST_RECORD = 100
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG,"onCreate:")
        setContentView(R.layout.activity_material)
        setSupportActionBar(toolbar)
        Log.d(TAG,"secret:${secretNumber.secret}")

        val count = getSharedPreferences("guess", Context.MODE_PRIVATE)
            .getInt("REC_count",-1)
        val nick = getSharedPreferences("guess", Context.MODE_PRIVATE)
            .getString("REC_nickname",null)
        Log.d(TAG,"data:"+count +nick)

        button_fab.setOnClickListener { view ->
            replay()
        }
        counter.setText(secretNumber.count.toString())
    }

    private fun replay() {
        AlertDialog.Builder(this)
            .setTitle("Replay game")
            .setMessage("Are you sure")
            .setPositiveButton(getString(R.string.ok), { dialog, which ->
                secretNumber.reset()
                counter.setText(secretNumber.count.toString())
                ed_number.setText("")
                Log.d(TAG, "secret:${secretNumber.secret}")
            })
            .setNeutralButton("Cancel", null)
            .show()
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG,"onStart:")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG,"onStop:")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG,"onPause:")
    }


    override fun onRestart() {
        super.onRestart()
        Log.d(TAG,"onRestart:")
    }


    override fun onResume() {
        super.onResume()
        Log.d(TAG,"onResume:")
    }


    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG,"onDestroy:")
    }

    fun check(view : View){
        val number = ed_number.text.toString().toInt()
        println("number:$number")
        Log.d(TAG,"number:$number")
        val diff = secretNumber.validate(number)
        var message = getString(R.string.yes_you_got_it)
        if (diff>0){
            message = getString(R.string.smaller)
        }else if(diff<0){
            message = getString(R.string.bigger)
        }
        counter.setText(secretNumber.count.toString())
        ed_number.setText("")
        //Toast.makeText(this,message,Toast.LENGTH_LONG).show()
        AlertDialog.Builder(this)
            .setTitle(getString(R.string.dialog_title))
            .setMessage(message)
            .setPositiveButton(R.string.ok,{dialog, which ->
                if(diff==0){
                    val intent = Intent(this,RecordActivity::class.java)
                    intent.putExtra("COUNTER",secretNumber.count)
                    //startActivity(intent)
                    startActivityForResult(intent,REQUEST_RECORD)
                }
            })
            .show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == REQUEST_RECORD){
            if(resultCode == Activity.RESULT_OK){
                val nickname = data?.getStringExtra("NICK")
                Log.d(TAG,"onActivityResult:$nickname")
                replay()
            }
        }

    }
}

